# README #

#### 2ND project/reportory:####
LiNK TO ACESS
https://sourceforge.net/p/devops-20-21-lmn-1030925/1030925/ci/default/tree/

___________________________________________________________________
___________________________________________________________________
### Principals Notes: ###  

* Get the original files of spring-guides/tut-react-and-spring-data-rest  
git clone https://github.com/spring-guides/tut-react-and-spring-data-rest  

* Run the REACT Server  
cd tut-react-and-spring-data-rest/basic  
cd CA1/basic  

* then  
./mvnw spring-boot:run  
 To stop the server - Ctlr + C  

* Git help  
git help add  

* Main steps to a initial commit in a new reportory:  
git init  
git add -A    (git add .)  
git commit -m 'Added my project'  
git remote add origin git@github.com:sammy/my-new-project.git  
git push -u -f origin master  

* Delete a file/folder  
git rm teste.txt  

* Procediment after made a first commit  
git add .  
git commit 'descrição'  
git push  


## Global configurations ##  
* Check all configurations  
git config - l  -   

* Asign a specific text editor  
git config --global core.editor "nano"  

* Asign a specific text editor - Notepad++ (not works in my laptop)  
Não funciona = git config --global core.editor "’C:/Program Files/Notepad++/notepad++.exe’ -multiInst -nosession"  

* Check checksum number hash  
git log -   

___________________________________________________________________
___________________________________________________________________

## TUTORIAL for the Pratical Classroom 2 16/03/2022 ##

#### 1 - check you global configurations 
git config - l

#### 2 - Correct you personal data if necessary 
git config --global user.name "José Faria"  
git config --global user.email 1030925@isep.ipp.pt

#### 3 - Config your editor prefered 
git config --global core.editor "nano"  

#### 4 - Check git log - previous checkums    
git log

#### 5 - Is necessary realize a commit  
git commit 'First commit 15/03'

#### 6 - Made the first push  
git push

#### 7 - Start the project Pratical Classroom 2 - See the files/folders in directory  
dir  
ls -la

#### 8 - copy the folder (all) 'tut-react-and-spring-data-rest' to a new folder copy 'CA1'  
cp -a tut-react-and-spring-data-rest CA1

#### 9 - Made the second commit  
git add -A   
git commit -m 'second commit with CA1 copied'  
git push  

#### 10 - Check Directory  
ls -la

#### 11 - Tag initial version as 1.1.0  
git tag -a v1.1.0 -m "my first version 1.1.0"  
git push origin v1.1.0

#### 12 - check git global configs
git config -l

#### 13 - Made all changes in files for adding jobYears in IntelliJ.  
git tag -a v1.2.0 -m "my second version with jobYears done"  
git push origin v1.2.0

#### 14 - Change version v.1.2.0 to v.ca1-part1  
git tag -a vca1-part1 -m "Final version - Part1 with jobYears done"  
git push origin vca1-part1  

#### 15 - Delete local and remote tags version v.1.2.0  and v.ca1-part1  
git tag -d vca1-part1 
git tag --delete vca1-part1 
git push --delete origin vca1-part1
git tag -d v1.2.0
git push --delete origin v1.2.0
git tag --delete vca1-part1 
 
#### 16 - Made the second commit  
git add -A   
git commit -m 'second commit with CA1 copied'  
git push 

#### 17 - Assign a new tag  
git push origin vca1-part1  


# FINNISHED!!!!!

___________________________________________________________________
___________________________________________________________________

## TUTORIAL for the Pratical Classroom 2 - Part 2 23/03/2022 ##
#### 1 - Detele branch created by default named 'main'
git branch -d 'main'

#### 2 - Check Git status
git status

#### 3 - Create new branch named "email-field"
git branch email-field
git checkout email-field

#### 4 - Test add and commit branch remotlly and checkout
git push --set-upstream origin email-field
git checkout
"Your branch is up to date with 'origin/email-field'."

#### 5 - Change all files to allow to apear the field E-mail Employeer
git add -A   
git commit -m 'First commit with field Email Empoyee'  
git push 
(not see yet the divergence..)

#### 6 - Force to create a divergente e-mail-field <=> master, by changing to branch "master"
git checkout master
git add -A   
git commit -m 'Readme.md updated'  
git push 

#### 7 - See divergence in GIT Web and via Git
git log --oneline --decorate --graph --all

#### 8 - Merge to Master
git checkout
git merge email-field
optional - delete "email-field branch": git branch -d 'email-field'

#### 9 - Commit the merged branchs e-mail-field <=> master
git checkout master
git add -A   
git commit -m 'Merge to Master Branch done''  
git push 
git log --oneline --decorate --graph --all

#### 10 - Tag for version as 1.3.0 
git tag -a v1.3.0 -m "2nd part of work role - version 1.3.0"  
git push origin v1.3.0 

#### 11 - Check status Git
git status

#### 12 - Create new branch named "fix-invalid-email"
git branch fix-invalid-email
git checkout fix-invalid-email

#### 13 - Test add and commit branch remotlly and checkout
git push --set-upstream origin fix-invalid-email
git checkout
"Your branch is up to date with 'origin/fix-invalid-email'."

#### 14 - Commit the the new email updated FIX
git add -A   
git commit -m 'New e-mail bug fixed (Works with e-mails without "@" and "."'  
git push 
git log --oneline --decorate --graph --all

#### 15 - Merge to Master <=> fix-invalid-email
git checkout
git checkout master
git merge fix-invalid-email
optional - delete "fix-invalid-email branch": git branch -d 'fix-invalid-email'
git log --oneline --decorate --graph --all

#### 16 - Commit the merged branchs Master <=> fix-invalid-email
git add -A   
git commit -m 'Merged fix-invalid-email branch to Master branch'  
git push 
git log --oneline --decorate --graph --all

#### 17 - Conflit detected - file README.md
git status
$ git add ../../README.md
git commit -m 'Conflict with README.md solved'
git push
git log --oneline --decorate --graph --all

#### 18 - Tag for version as 1.3.1 
git tag -a v1.3.0 -m "2nd part of work role - version 1.3.1"  
git push origin v1.3.1

#### 19 - Tag for version as ca1-part2 
git tag -a vca1-part2 -m "2nd part of work role - version ca1-part2"  
git push origin vca1-part2

#### 20 - Updated the final version of README.md
git add -A   
git commit -m 'README.md updated'  
<<<<<<< HEAD
git push 

# FINNISHED!!!!!



___________________________________________________________________
___________________________________________________________________
# SOURCEFORGE: #


# README #

IMPORTANT NOTE:
I can't do commit with original CA1 by the cmd (via mercurial) presents the next error:

hg commit -m 'CA1'
trouble committing CA1\basic\node_modules\@babel\helper-builder-binary-assignment-operator-visitor\LICENSE!
transaction abort!
rollback completed
abort: D:\OneDrive - Instituto Superior de Engenharia do Porto\_SWITCH 2021\Projecto\devops-20-21-lmn-1030925 Source v4\devops-20-21-lmn-1030925-1030925/.hg/store/data/_c_a1/basic/node__modules/@babel/helper-builder-binary-assignment-operator-visitor/_l_i_c_e_n_s_e.i: The system cannot find the path specified

So was do all steps/branchs with dummies files inside of CA1 folder.

___________________________________________________________________
___________________________________________________________________
___________________________________________________________________


### Principal resume by choosing SOURCEFORGE ###  

All steps for sourceforge was do by using mercurial via cmd.
Was be created a new login account in sourceforge

Why was choose sourceforge?
First have support by using mercurial allowing commiting by via command or by using GUI.
Between bitbucket and sourceforge/mercurial, the sourceforge/mercurial enviroment is not so friendly, specially i get some bugs/issues by doing commits (read the README upside).

Is not so friendly, specially by need a bridge between local PC and SourceForge, and have some bugs/fails several time the communications between local em remote server.

___________________________________________________________________
___________________________________________________________________


### Principals Notes: ###  

* Clone the original https://sourceforge.net/p/devops-20-21-lmn-1030925/1030925/ci/default/tree/
hg clone ssh://jlamfaria@hg.code.sf.net/p/devops-20-21-lmn-1030925/1030925 devops-20-21-lmn-1030925-1030925 

* Hg help  
hg help  

* Main steps to a initial commit in a new reportory:  
hg add  
hg commit -m 'Message'  
hg push

* Delete a file/folder  
hg rm teste.txt  

## Global configurations ##  
* Check all configurations  
hg config --edit 
___________________________________________________________________
___________________________________________________________________

## TUTORIAL for the Pratical Classroom 2 16/03/2022 ##

#### 1 - Clone the sourceforce directory
hg clone ssh://jlamfaria@hg.code.sf.net/p/devops-20-21-lmn-1030925/1030925 devops-20-21-lmn-1030925-1030925 

#### 2 - Check git log
hg log

#### 3 - Made the first commit
hg add
hg commit -m 'CA1 folder with some dummies files'
hg add

#### 4 - Start the project Pratical Classroom 2 - CA1 - See the files/folders in directory  
cd \CA1

#### 5 - Check Directory  
ls -la

#### 6 - Tag initial version as 1.1.0  
hg tag v1.1.0
hg commit -m 'Commit tag v1.1.0'
hg push

#### 7 - change a Readme File Dummy as editing a few JAVA files.  
hg tag v1.2.0 -m "my second version with jobYears done"  
hg add
hg commit -m 'My second version with jobYears done'
hg push

#### 8 - Remove tag
hg tag --remove v1.2.0

#### 9 - Change version v.1.2.0 to v.ca1-part1  
hg tag --remove v1.2.0
hg tag vca1-part1   -m "Final tag Part1"  
hg push origin vca1-part1  

# FINNISHED!!!!!

___________________________________________________________________
___________________________________________________________________

## TUTORIAL for the Pratical Classroom 2 - Part 2 23/03/2022 ##hb 

#### 1 - Create new branch named "email-field"
hg branch email-field
hg push --new-branch

#### 2 - Change all files to allow to apear the field E-mail Employeer
hg add   
hg commit -m 'First commit with field Email Empoyee'  
hg push 

#### 3 - Merge to Master
hg checkout
hg update default
hg add
hg commit -m "Merge with default with email-field"
hg push

#### 4 - Tag for version as 1.3.0 
hg tag v1.3.0   -m "Final tag v1.3.0"  
hg push origin v1.3.0 

#### 5 - Create new branch named "fix-invalid-email"
hg branch fix-invalid-email
hg push --new-branch


#### 6 - Commit the the new email updated FIX
hg add   
hg commit -m 'New e-mail bug fixed (Works with e-mails without "@" and "."'  
hg push 

#### 7 - Merge to Master <=> fix-invalid-email
hg checkout
hg update default
hg add
hg commit -m "Merge with default with fix-invalid-email"
hg push

#### 8 - Tag for version as ca1-part2 
hg tag vca1-part2    -m "Final tag vca1-part2"
hg push origin vca1-part2 

# FINNISHED!!!!!
=======
git push 
>>>>>>> d07c169120fd252fc488413c4e4dcb2c087be625

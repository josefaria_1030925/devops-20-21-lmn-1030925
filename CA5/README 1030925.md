
TUTORIAL CA5 - Part1 
===================

**1** - Open a Terminal Windows and introduce the next command to create a new docker image:
    
    docker run -u root --rm -d -p 8080:9090 -p 50000:50000 -v $HOME/jenkins:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock jenkinsci/blueocean

or

    java -jar jenkins.war --enable-future-java --httpPort=8090

**2** - Execute the next commands in docker:

    docker images

**3** - Select the image to run (Jenkins):

    docker run jenkinsci/blueocean

**4** - To get the password to proceed to instalations we need to open a new termina, because the image its running:

    docker ps

    docker exec -it a7a16bd561c3 /bin/bash

    cat /var/jenkins_home/secrets/initialAdminPassword

**5** - Password to acess to Jenkins instalations:

    01d03c3f3adc4b07a6fa3b21c80d588e

**6** - Open the broswer and start the Jenkins instalation:

    http://localhost:8080/

**7** - Login:

    User: admin
    Pass: admin

**8** - After entered in Jekins config we need to do some configurations:

    Go to Jenkins -> Dashboard -> New Item -> Name: DEVOPS CA5 Part1 -> Pipeline

**9** - Create a new script based in JenkinsFile for several tasks (with comments in each script):

    pipeline {
    agent any

    //Task for get the project from my bitbucket    

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git branch: 'master', url: 'https://bitbucket.org/josefaria_1030925/devops-20-21-lmn-1030925'
            }
        }

    //Task for execute the assembly from gradlew file 

        stage('Assembly') {
            steps {
                echo 'Assembling...'
                dir('CA2/Part1') {
                      script {
                     if (isUnix())
                       sh './gradlew clean assemble'
                         else
                       bat './gradlew clean assemble'
                   }
                }
                echo 'Done Assembly'

            }
        }

         //Task for running all tests from gradlew file 

        stage('Test') {
            steps {
                echo 'Testing...'
                dir('CA2/Part1') {
                    script {
                       if (isUnix())
                    sh '../gradlew test'
                       else
                    bat './gradlew test'
                    }
                }
                echo 'Done Tests'

            }
        }

        //Task for doing a compressed file (as backup) from gradlew file 

        stage('Archive') {
            steps {
                echo 'Archiving...'
                dir('CA2/Part1') {
                    archiveArtifacts 'build/distributions/*'
                }
                echo 'Done Archive'
            }
        }
    }

        //Generate a report
        
        post {
            always {
                junit 'CA2/Part1/build/test-results/test/*.xml'
            }
        }
    }

**10** - Run the Jenkin build to check if all steps pass with sucesseful.

**11** - Created a new test script as example:

    public void SimpleTestoneEqualsOne() {
        int expected = 1;
        int result = 1;
        assertEquals(expected, result);
    }

**12** - Commit all changes

    git add -A
    git commit -m "Updated README.md CA5-Part1 (resolves #16)"
    git push
    git tag ca5-part1
    git push origin ca5-part1


TUTORIAL CA5 - Part2
===================
**1** - Made a copy of **gradle_basic_demo** to folder **CA5/Part2** and commit

    git add -A
    git commit -m 'Made a copy of gradle_basic_demo (addresses #17)'
    git push

**2** - In Terminal run the Jenkins image

    java -jar jenkins.war

**3** - Add the nexts plugins in Jenkins - Manage Jenkins -> Manage Plugins -> Available ->
    
    HTML Publisher
    Docker Pipeline

**4** - Now we need to create the scripts for the **HTML Publisher**  and **Docker Pipeline** in Jenkins files:

         stage('Generate Jar File') {
              steps {
                     echo 'Generating jar...'
                     dir('CA5/Part2/gradle_basic_demo') {
                        bat './gradlew jar'
                     }
                    echo 'Jar file generated'
              }
         }

         stage('Javadoc') {
               steps {
                      echo 'Creating Javadoc file...'
                      dir('CA5/Part2/gradle_basic_demo') {
                         bat './gradlew javadoc'
                      }
                      echo 'Javadoc file created'
               }
         }

        post
        {
            always
            {
                junit allowEmptyResults: true, testResults:'CA5/Part2/gradle_basic_demo/build/test-results/test/*.xml'
                            publishHTML([allowMissing: false,
                                alwaysLinkToLastBuild: false,
                                keepAll: false,
                                reportDir: 'CA5/Part2/gradle_basic_demo/build/docs/javadoc',
                                reportFiles: 'index.html',
                                reportName: 'HTML Report',
                                reportTitles: 'The Report'])
            }
        }

**5** - We need to set the credentials in Jenkins - Mannage Jenkins -> Manage credentials -> Global -> Add Credentials

    Kind: Username with password
    Scope: Global
    Username: from docker hub
    Password: from docker hub
    ID: dockerhub_credentials

**6** - Create Dockerfile (use the file from CA4/Part-1/Version-2):


**7** - First do a Build in Gradlew:

    ./gradlew build


**8** - Build a docker image from Dockerfile:

    docker build -t ca5_part2 .


**9** - Its need to updtated the Jenkinfile with a script for Docker Iamge:

        stage ('Docker Image Generation')
        {
                     steps
                     {
                         echo 'Generating Docker image...'
                         dir('CA5/Part2/gradle_basic_demo')
                         {
                             script {
                                 docker.withRegistry('https://registry.hub.docker.com', 'dockerhub_credentials')
                                      {
                                        def customImage = docker.build("jlamfaria/ca5_part2:${env.BUILD_ID}")
                                        customImage.push()
                                      }
                                    }
                         }
                         echo 'Docker image generated'
                     }
        }


**10** - Now we need to run the container with **port 59001** and match with the port of host machine - **59001**

    docker run -p 59001:59001 -d ca5_part2

**11** - Now we need to push the image docker to **https://hub.docker.com**

    docker tag ca5_part2 jlamfaria/ca5_part2:ca5_part2

    docker push jlamfaria/ca5_part2:ca5_part2

**12** - In Jenkins Page configure a new Item = Dashboard -> New Item -> Name: CA5-PART2 -> Pipeline

**13** - Than configure the Pipeline = Pipeline -> Definition: Pipeline script from SCM

    SCM: Git
    Repository URL: https://bitbucket.org/josefaria_1030925/devops-20-21-lmn-1030925
    Branch: */master
    Script path: CA5/Part2/gradle_basic_demo/Jenkinsfile
    
    Save

**14** - Now **BuildNow** to check if all running well.

**15** -![CA5 Part2.jpg](CA5 Part2.jpg)

**16** - Doing anothe commit after several corrections:

    git add -A
    git commit -m 'Created Dockerfile and updated Jenkinsfile with new script (resolves #18)'
    git push

**17** - Commit the changes a

    git add -A
    git commit -m "Finnished README.md CA5 Part2 and separate README.md files per parts (resolves #19)"
    git push
    git tag ca5-part2
    git push origin ca5-part2
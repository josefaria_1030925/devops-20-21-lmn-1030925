package basic_demo;

import static org.junit.Assert.*;

import org.junit.Test;

public class AppTest {
    @Test
    public void testAppHasAGreeting() {
        App classUnderTest = new App();
        assertNotNull("app should have a greeting", classUnderTest.getGreeting());
    }

    @Test
    public void SimpleTestoneEqualsOne() {
        int expected = 1;
        int result = 1;
        assertEquals(expected, result);
    }
}



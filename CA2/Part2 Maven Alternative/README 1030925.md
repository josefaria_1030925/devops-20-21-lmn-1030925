
TUTORIAL CA2 - Part1
===================

**1** - Download via Git Bash - git clone https://JoseFaria_1030925@bitbucket.org/luisnogueira/gradle_basic_demo.git

**2** - Delete  folder ".git"

**3** - In the root of folder CA2\Part1 run bia Git bash:

    ./gradlew build

**4** - Run the server - Port 59001:

    java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

**5** - Run the client A application:

    ./gradlew runClient

**6** - Named username as "ClientA"
**7** - Chat "Hello World"

**8** - Run the client B application:

    ./gradlew runClient

**9** - Named username as "ClientB"
**10** - Chat "Goodbye World"

**11** - Create the first issue #1 " Add a new task to execute the server. "

**12** - Now we will type the task to run automatlly the server
**13** - In file build.gradle we need to creat a new task:

    task startServer(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches a chat server that connects to a server on localhost:59001 "

    classpath = sourceSets.main.runtimeClasspath

    mainClass = 'basic_demo.ChatServerApp'

    args '59001'
    }

**14** - Then build and run

    ./gradlew build
    ./gradlew startServer

**15** - Then commit and close first issue #1 " Add a new task to execute the server. "

**16** - Add a simple unit test and update the gradle script so that it is able to execute the
test.

**17** - Create the folders needs for tests
**18** - Create a new file named as AppTest.java  and type the next code:

    package basic_demo;
    import static org.junit.Assert.*;
    import org.junit.Test;

    public class AppTest {
    @Test
    public void testAppHasAGreeting() {
    App classUnderTest = new App();
    assertNotNull("app should have a greeting", classUnderTest.getGreeting());
    }
    }

**19** - In build.gradle implements the next dependenciy:
    
    testImplementation 'junit:junit:4.12'

**20** - Run the test:

    ./gradlew test

**21** - Then commit and close first issue #4 "AppTest.java done and running - resolve #4"

**22** - Add a new task of type Copy to be used to make a backup of the sources of the
application.

**23** - Creates a new issue "#5: Add a new task of type Copy to be used to make a backup of the sources of the application"

    Is not necessary to create the backup folder in the root of CA1\Part1

**24** - In build.gradle implements the next task:

    task copySrc(type: Copy) {
    from 'src'
    into 'backup'
    }

**25** - Run the task:

    ./gradlew copySrc

**26** - Then commit and close first issue #5 "Task "copySrc" copy Src folder to backup folder (resolve #5)"

**27** - Add a new task of type Zip to be used to make an archive (i.e., zip file) of the
sources of the application.

**28** - Creates a new issue "#6: Add a new task of type Zip to be used to make an archive (i.e., zip file) of the 
sources of the application."

**29** - Again is not necessary to create the backup folder in the root of CA1\Part1

**30** - In build.gradle implements the next task:

    task zipSrc(type: Zip) {
    archiveFileName = 'srcCA1Part1.zip'
    from 'src'
    destinationDirectory = file('backup')
    }

**31** - Run the task:

    ./gradlew zipSrc

**32** - Note: If the file "srcCA1Part1.zip" exists inside folder, the task run but not overwrite the file.
Could be necessary use a future task like:

    task deleteZipSrc(type: Delete) {
    delete("$backup\srcCA1Part1.zip")
    }


**33** - Then commit and close first issue #6 "Task "zipSrc" works with full backup of src in ZIP Extension (resolve #6)"

**34** - Create the next tag vca2-part1

    git tag vca2-part1    -m "Final tag vca2-part1"

**35** - Push the tag for the last commit

    git push origin vca2-part1

FINNISH CA2 - PART1


TUTORIAL CA2 - Part2
===================
**1** - Create new branch named "tut-basic-part2-maven"

    git branch tut-basic-part2-maven
    git push --set-upstream origin tut-basic-part2-maven
    git checkout tut-basic-part2-maven

**2** - mkdir

    mkdir D:\OneDrive - Instituto Superior de Engenharia do Porto\_SWITCH 2021\Projecto\devops-20-21-lmn-1030925\CA2\Part2

    Follow the PDF instructions about get and extract the file "gradle spring boot project" 
    available in the link link https://start.spring.io/

**3** - First commit for CA2
    
    git add -A
    git commit -m 'Initial files with part2 folder, gradle spring boot project  and readme.md' 
    git push

**4** - Remove the src folder

    rm -R src/

**5** - Copy the src folder from tut-react-and-spring-data-rest/basic/ to folder part2

    cp -r ~/DEVOPS/tut-react-and-spring-data-rest/basic/src/ ~/DEVOPS/devops-21-22-lmn-1211754/CA2/part2/

**6** - Copy the package.json and webpack.config.js files
    
    cp package.json ~/DEVOPS/devops-21-22-lmn-1211754/CA2/part2/
    cp webpack.config.js ~/DEVOPS/devops-21-22-lmn-1211754/CA2/part2/

**7** Delete de folder src/main/resources/static/built
    
    rm -R src/main/resources/static/built

**8** Run the apliccation with next commands:

    ./mvnw springboot:run
    check in next link the page  http://localhost:8080/ and it showed empty

**8** Download the Maven package from guthub - https://github.com/eirslett/frontend-maven-plugin
    
    add plugin to pom.xml file with necessary executions (lines 55 to 95)

**9** Updated the scripts in package.json file

**10** Run de Maven application
    
    ./mvnw springboot:run

    (ERROR in builds. I don't identify the reason)
    if the build was run well i need to see in http://localhost:8080/

**11** Create a task to delete all files generated 

    add plugin to pom.xml file with necessary executions (lines 123 to 134)

**12** Creates a task to copy all genereated jar files to a folder named "dist"

    add plugin to pom.xml file with necessary executions (lines 99 to 123)

**13** Experimentinf the created features (Not run by erros occured in builds):
    
    ./mvnw install
    ./mvnw clean

**14** Committing changes final version

    git add -A
    git commit -m "Maven alternative implementations with error in builds (resolve #8)"
    git push

**15** Create the next tag vca2-part2-alternative

    git tag vca2-part2-alternative    -m "Final tag vca2-part2-alternative"

**16** Push the tag for the last commit

    git push origin vca2-part2-alternative

FINNISH CA2 - PART2
==================================================================================================================================
*Report about Gradle and Maven*

## 1. Introduction to Gradle

Gradle is an advanced device focused on Groovy and Kotlin for general purposes. It is an open-source construction 
automation platform based on Apache Maven and Apache Ant concepts. It can create virtually any kind of software. 
This is for the multi-project construction that can be very growing. Instead of XML to announce project configuration, 
it introduces a Java and Groovy-based DSL. It uses a DAG to set the order of the task to be performed. Gradle provides
an elastic model that can assist in creating and packaging code creation cycles for web and mobile applications.

It provides support for software development, testing, and deployment on various platforms. The automation framework is 
designed for several languages and platforms, including Java, Scala, Linux, C / C++, and Groovy. Gradle was first 
published in 2007 and is updated stably on 18 November 2019. Gradle has taken the advantages of Ant and Maven, and the
two have been eliminated.

**Advantages of using Gradle:**
- General-purpose build tool: Commonly designed to build applications of any kind.
- Highly Customizable: It can be personalized and extended. This can be tailored under various technologies for 
different projects. It can be modified in several ways, as in projects like Java, Groovy, and more.
- Performance: Gradle is very fast and efficient. In all cases, it’s about twice the speed of Maven and hundreds of 
times the speed of building cache.
- Flexibility: It is a flexible instrument. Gradle is a tool for creating plug-ins. In the different languages of
programming, such as Java, Groovy, Kotlin, Scala, and others, we can build our plugin. To do so, we can build a plugin
and monitor the codebase if we would like to add any functionalities to the project after deployment.
- User Experience: It provides a wide variety of IDEs for improved user experience. While others prefer to work on the 
IDE, Gradle gives you a control-line interface; other users prefer to work at the terminal.

**Disavantages of using Gradle:**
- IDE integration is not so frienfly in Gradle.
- Gradle doesn’t come with built-in, plugins ant project structure.
  So each project have the own and unique struct and build scripts.


## 2. Introduction to Maven 
Maven is an automation and management tool. Maven is a popular open source build tool for enterprise Java projects, 
esigned to take much of the hard work out of the build process. Maven uses a declarative approach, where the project 
tructure and contents are described, rather than the task-based approach used in Ant or in traditional make files, 
for example. This helps enforce company-wide development standards and reduces the time needed to write and maintain 
build scripts. When you use Maven, you describe your project using a well-defined project object model, Maven can then 
apply cross-cutting logic from a set of shared (or custom) plugins. In case of multiple development teams environment, 
Maven can set-up the way to work as per standards in a very short time. As most of the project setups are simple and 
reusable, Maven makes life of developer easy while creating reports, checks, build and testing automation setups.


**Advantages of using Maven:**
- Maven can add all the dependencies required for the project automatically by reading pom file.
- One can easily build their project to jar,war etc. as per their requirements using Maven.
- Maven makes easy to start project in different environments and one doesn’t needs to handle the 
dependencies injection, builds, processing, etc.
- Adding a new dependency is very easy. One has to just write the dependency code in pom file.

**Disavantages of using Maven:**
- Maven needs the maven installation in the system for working and maven plugin for the ide.
- If the maven code for an existing dependency is not available, then one cannot add that dependency using maven.

## 3. Gradle vs Maven

| Basis                  | Gradle                                                                                    | Maven                                                                                     | 
|------------------------|-------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------|
| Based on               | Gradle is based on developing domain-specific language projects.                          | Maven is based on developing pure Java language-based software.                           |
| Configuration          | It uses a Groovy-based Domain-specific language(DSL) for creating project structure.      | It uses Extensible Markup Language(XML) for creating project structure.                   |
| Focuses on             | Developing applications by adding new features to them.	                                  | Developing applications in a given time limit.                                            |
| Performance	          | It performs better than maven as it optimized for tracking only current running task.	  | It does not create local temporary files during software creation, and is hence – slower. |
| Java Compilation       | It avoids compilation.                                                                    | It is necessary to compile.                                                               |
| Usability              | It is a new tool, which requires users to spend a lot of time to get used to it.          | This tool is a known tool for many users and is easily available.                         |
| Customization          | This tool is highly customizable as it supports a variety of IDE’s.	                      | This tool serves a limited amount of developers and is not that customizable.             |
| Languages supported    | It supports software development in Java, C, C++, and Groovy.                             | It supports software development in Java, Scala, C#, and Ruby.                            |

## 4. Final Conclusion

At firt aproach to work with Gradle was a bit hard, by the lack of experience. I experienced several issues or bugs in gradle implementation,
but as gradle its works with scripts is more easy to understand. However requires a lot of research to find another scripts examples and adpat to 
our project. But like i said i believe its by my lack of experience.
But have the great advantage to automates a lot of tasks, when we can save a lot of time by implementation of futures versions of our project or futures
projects.

About maven i see its more difficult to works and implements another funcionallity, specially i can't finnish the alternativ part of Maven by find bugs in build/install
maven when i don't understand what its missing. In another works Maven its more "quirky" than Gradle. Also we find less tasks examples in maven then in Gradle in internt

So i believe the Gradle its better than Maven, specially in this moment gradle its more used the Maven in DEVOPS.



==================================================================================================================================
==================================================================================================================================
==================================================================================================================================

Gradle Basic Demo
===================

This is a demo application that implements a basic multithreaded chat room server.

The server supports several simultaneous clients through multithreading. When a client connects the server requests a screen name, and keeps requesting a name until a unique one is received. After a client submits a unique name, the server acknowledges it. Then all messages from that client will be broadcast to all other clients that have submitted a unique screen name. A simple "chat protocol" is used for managing a user's registration/leaving and message broadcast.


Prerequisites
-------------

 * Java JDK 8 (or newer)
 * Apache Log4J 2
 * Gradle 7.4.1 (if you do not use the gradle wrapper in the project)
   

Build
-----

To build a .jar file with the application:

    % ./gradlew build 

Run the server
--------------

Open a terminal and execute the following command from the project's root directory:

    % java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp <server port>

Substitute <server port> by a valid por number, e.g. 59001

Run a client
------------

Open another terminal and execute the following gradle task from the project's root directory:

    % ./gradlew runClient

The above task assumes the chat server's IP is "localhost" and its port is "59001". If you whish to use other parameters please edit the runClient task in the "build.gradle" file in the project's root directory.

To run several clients, you just need to open more terminals and repeat the invocation of the runClient gradle task